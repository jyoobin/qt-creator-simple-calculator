#include "grade_calculator.h"
#include "ui_grade_calculator.h"

grade_calculator::grade_calculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::grade_calculator)
{
    ui->setupUi(this);

    //QObject::connect(ui->comboBox)

    QObject::connect(ui->radioButton, SIGNAL(clicked()), this, SLOT(scheme1()));
    QObject::connect(ui->radioButton_2, SIGNAL(clicked()), this, SLOT(scheme2()));
}

grade_calculator::~grade_calculator()
{
    delete ui;
}



// if one of the radio button is clicked, the lcdNumber shows the result
void grade_calculator::scheme1()
{
    QString course = ui->comboBox->currentText();

    if (course == "PIC 10B. Intermediate Programming"){
        int homework = ui->HWSpin1->value() + ui->HWSpin2->value() + ui->HWSpin3->value() + ui->HWSpin4->value()
                + ui->HWSpin5->value() + ui->HWSpin6->value() + ui->HWSpin7->value() + ui->HWSpin8->value();

        int midterms = ui->MTSpin1->value() + ui->MTSpin2->value();

        int final = ui->FSpin->value();

        double percentage = (static_cast<double>(homework) /8 ) *0.25 + (static_cast<double>(midterms)/2) * 0.40 + static_cast<double>(final) * 0.35;

        ui->lcdNumber->display(percentage);
    }

    else if (course == "PIC 10C. Advanced Programming"){
        int homework = ui->HWSpin1->value() + ui->HWSpin2->value() + ui->HWSpin3->value() + ui->HWSpin4->value()
                + ui->HWSpin5->value() + ui->HWSpin6->value() + ui->HWSpin7->value() + ui->HWSpin8->value();

        int midterms = ui->MTSpin1->value() + ui->MTSpin2->value();

        int final = ui->FSpin->value();

        double percentage = (static_cast<double>(homework) /8 ) *0.10 + (static_cast<double>(midterms)/2) * 0.20 + static_cast<double>(final) * 0.70;

        ui->lcdNumber->display(percentage);
    }
}


void grade_calculator::scheme2()
{
    QString course = ui->comboBox->currentText();

    if (course == "PIC 10B. Intermediate Programming"){
        int homework = ui->HWSpin1->value() + ui->HWSpin2->value() + ui->HWSpin3->value() + ui->HWSpin4->value()
                + ui->HWSpin5->value() + ui->HWSpin6->value() + ui->HWSpin7->value() + ui->HWSpin8->value();

        int midterms;
        ui->MTSpin1->value() > ui->MTSpin2->value() ? midterms = ui->MTSpin1->value() : midterms = ui->MTSpin2->value();

        int final = ui->FSpin->value();

        double percentage = (static_cast<double>(homework) /8 ) *0.25 + static_cast<double>(midterms) * 0.30 + static_cast<double>(final) * 0.44;

        ui->lcdNumber->display(percentage);
     }
    else if (course == "PIC 10C. Advanced Programming"){
        int homework = ui->HWSpin1->value() + ui->HWSpin2->value() + ui->HWSpin3->value() + ui->HWSpin4->value()
                + ui->HWSpin5->value() + ui->HWSpin6->value() + ui->HWSpin7->value() + ui->HWSpin8->value();

        int midterms;
        ui->MTSpin1->value() > ui->MTSpin2->value() ? midterms = ui->MTSpin1->value() : midterms = ui->MTSpin2->value();

        int final = ui->FSpin->value();

        double percentage = (static_cast<double>(homework) /8 ) *0.10 + static_cast<double>(midterms) * 0.40 + static_cast<double>(final) * 0.50;

        ui->lcdNumber->display(percentage);
    }
}
