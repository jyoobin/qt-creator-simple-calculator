#ifndef GRADE_CALCULATOR_H
#define GRADE_CALCULATOR_H

#include <QMainWindow>

namespace Ui {
class grade_calculator;
}

class grade_calculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit grade_calculator(QWidget *parent = 0);
    ~grade_calculator();

private:
    Ui::grade_calculator *ui;

signals:
    void combobox_changed();

private slots:
    void scheme1();
    void scheme2();
};

#endif // GRADE_CALCULATOR_H
